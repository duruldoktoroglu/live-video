const express = require('express');
const path = require('path');

module.exports = class Routes {

    constructor(app) {
        this.app = app;
        this.setStaticDir(); // new
    }

    home() {
        this.app.get('/', (request, response) => {
            response.sendFile('index.html');
        });
    }

    rooms() {
        this.app.get('/room/:roomId', (request, response) => {
            response.sendFile(path.join(__dirname, '../views', 'room.html'));
        });
    }

    // new
    setStaticDir() {
        this.app.use(express.static(path.join(__dirname, '../views')));
    }

    getRoutes() {
        this.home();
        this.rooms();
    }
}