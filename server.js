const ioServer = require('socket.io');
const RTCMultiConnectionServer = require('rtcmulticonnection-server');

var path = require('path');
var express = require('express');
var app = express();
var http = require( "http" ).createServer( app );

ioServer(http).on("connection", (socket) => {
    console.log("Connection enabled", socket.id);
    RTCMultiConnectionServer.addSocket(socket);
});

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, "src", "views", "index.html"));
});

app.get('/room/:roomId', function(req, res) {
    res.sendFile(path.join(__dirname, "src", "views", "room.html"));
});

app.use('/public', express.static(path.join(__dirname, './public')));

http.listen(5000, "127.0.0.1");

module.exports = app;